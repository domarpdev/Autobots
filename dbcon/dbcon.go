package dbcon

import (
	"bufio"
	"database/sql"
	"fmt"
	"os"
	"strings"

	// GO MySQL Driver
	_ "github.com/go-sql-driver/mysql"
)

//DBObject ...object for database access
var DBObject *sql.DB

// Configuration ...structure for tascdb_login.dbs
type Configuration struct {
	host     string
	port     string
	user     string
	password string
	database string
}

// ConnectToDB ...Connects to the database
func ConnectToDB() {
	config := readConfigFile()

	db, err := sql.Open("mysql", config.user+":"+config.password+"@tcp("+config.host+":"+config.port+")/"+config.database)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Connected to Database:", config.database)

	DBObject = db
}

// Reads database configuration file
func readConfigFile() Configuration {
	var config Configuration
	configPath := os.Getenv("imacs_path")

	configFile, err := os.Open(configPath)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer configFile.Close()

	reader := bufio.NewScanner(configFile)
	var dbConfig []string

	for reader.Scan() {
		dbConfig = append(dbConfig, reader.Text())
	}

	configFile.Close()

	config.host = strings.TrimSpace(strings.Split(dbConfig[0], ":")[1])
	config.port = strings.TrimSpace(strings.Split(dbConfig[4], ":")[1])
	config.user = strings.TrimSpace(strings.Split(dbConfig[6], ":")[1])
	config.password = strings.TrimSpace(strings.Split(dbConfig[7], ":")[1])
	config.database = strings.TrimSpace(strings.Split(dbConfig[5], ":")[1])

	fmt.Println("DB Config File Loaded:", configPath)
	return config
}
