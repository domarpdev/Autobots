package db

import (
	"Autobots/dbcon"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//HeaterInfo ...Structure for Heater Info
type HeaterInfo struct {
	HtrNo        string
	GrpNo        string
	HtrOnCmd     string
	HtrOffCmd    string
	HtrMnemonic  string
	HtrName      string
	SnsrMnemonic string
	SnsrName     string
}

//HeaterInfoList ...List of Heaters
var HeaterInfoList []HeaterInfo

//UpdateHeaterCmdTM ...Creates SQL table for Htrs
func UpdateHeaterCmdTM() {
	file, err := os.Open("/home/Pramod/dbs/tc-12r/thermal.tc")
	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()

	reader := bufio.NewScanner(file)
	htrNo := 1
	htrGrp := "0"
	skip := false

	for reader.Scan() {
		line := reader.Text()
		htrCmd := strings.TrimSpace(line[:15])
		if strings.Contains(htrCmd, "Htr-Grp") == true {
			htrGrp = strings.Split(htrCmd, "-")[2]
			continue
		} else if skip == true {
			skip = false
			continue
		} else {
			htrName := htrCmd[:len(htrCmd)-2]
			_, err = dbcon.DBObject.Exec("INSERT INTO `hw_htr_info` (`htr_no`, `grp_no`, `htr_on_cmd`, `htr_off_cmd`, `htr_sts`, `description`)" +
				"VALUES (" + strconv.Itoa(htrNo) + ", " + htrGrp + ", '" + htrName + "ON', '" + htrName + "OF', '" + htrName + "Sts', '" + htrName + "')")
			htrNo++
			skip = true
		}
	}
}

//GetHeatersInfo ...Load Heaters Info
func GetHeatersInfo() {
	result, err := dbcon.DBObject.Query("SELECT htr_no, grp_no, htr_on_cmd, htr_off_cmd, htr_sts, snsr_type, snsr_no, description FROM `hw_htr_info`")
	if err != nil {
		fmt.Println(err)
		return
	}

	for result.Next() {
		var heater HeaterInfo
		var snsrType, snsrNo string
		result.Scan(&heater.HtrNo, &heater.GrpNo, &heater.HtrOnCmd, &heater.HtrOffCmd, &heater.HtrMnemonic, &snsrType, &snsrNo, &heater.HtrName)

		cmdQuery, _ := dbcon.DBObject.Query("SELECT `CommandCode` FROM `tc_oldtypes` WHERE `Mnemonic` = '" + heater.HtrOnCmd + "'")
		if cmdQuery.Next() {
			cmdQuery.Scan(&heater.HtrOnCmd)
		}
		cmdQuery.Close()

		cmdQuery, _ = dbcon.DBObject.Query("SELECT `CommandCode` FROM `tc_oldtypes` WHERE `Mnemonic` = '" + heater.HtrOffCmd + "'")
		if cmdQuery.Next() {
			cmdQuery.Scan(&heater.HtrOffCmd)
		}
		cmdQuery.Close()

		result1, _ := dbcon.DBObject.Query("SELECT snsr_tm, description FROM `hw_tmp_snsr_info` WHERE snsr_type = '" + snsrType + "' AND snsr_no = " + snsrNo + "")

		if result1.Next() {
			result1.Scan(&heater.SnsrMnemonic, &heater.SnsrName)
		}

		result1.Close()

		HeaterInfoList = append(HeaterInfoList, heater)
	}
}

// GetHeaterMnemonics : Returns Heater Mnemonics
func GetHeaterMnemonics() []string {
	var heaters []string

	for _, htr := range HeaterInfoList {
		heaters = append(heaters, htr.HtrMnemonic)
	}

	return heaters
}
