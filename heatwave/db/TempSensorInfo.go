package db

import (
	"Autobots/dbcon"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//TempSensorInfo ...Structure for Temp Sensor Info
type TempSensorInfo struct {
	SnsrNo       string `json:"snsr_no"`
	SnsrType     string `json:"snsr_type"`
	SnsrMnemonic string `json:"snsr_mnemonic"`
	LowerLimit   string `json:"lower_limit"`
	UpperLimit   string `json:"upper_limit"`
	SnsrRef      string `json:"snsr_ref"`
	Description  string `json:"desc"`
	Value        string `json:"val"`
}

//TempSensorInfoList ...List of Temp Sensors
var TempSensorInfoList []TempSensorInfo

//UpdateTempSensorTM ...Creates SQL table for Htrs
func UpdateTempSensorTM() {
	file, err := os.Open("/home/Pramod/dbs/tm-12r/thermal.tm")
	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()

	reader := bufio.NewScanner(file)

	for reader.Scan() {
		line := reader.Text()
		stringSplit := strings.Fields(line[25:])
		var snsrType string
		pid := strings.TrimSpace(line[0:5])

		if pid[0] == 'P' {
			snsrType = "2"
		} else if pid[0] == 'T' {
			snsrType = "1"
		}

		snsrNoInt, _ := strconv.Atoi(pid[1:])
		snsrNo := strconv.Itoa(snsrNoInt)
		tmpSnsrTM := strings.TrimSpace(line[5:20])

		lowerLimit := stringSplit[len(stringSplit)-3]
		upperLimit := stringSplit[len(stringSplit)-4]
		tabRef := stringSplit[len(stringSplit)-2]
		description := strings.Replace(strings.Replace(tmpSnsrTM, "-", "", -1), "Tmp", "", 1)

		_, err = dbcon.DBObject.Exec("INSERT INTO `hw_tmp_snsr_info` (`snsr_no`, `snsr_type`, `snsr_tm`, `lower_limit`, `upper_limit`, `snsr_tab_ref`, `description`)" +
			"VALUES (" + snsrNo + ", " + snsrType + ", '" + tmpSnsrTM + "', " + lowerLimit + ", " + upperLimit + ", '" + tabRef + "', '" + description + "')")

	}
}

//GetTempSensorsInfo ...Load Temp Sensors Info
func GetTempSensorsInfo() {
	result, err := dbcon.DBObject.Query("SELECT snsr_no, snsr_type, snsr_tm, lower_limit, upper_limit, snsr_tab_ref, description FROM `hw_tmp_snsr_info`")
	if err != nil {
		fmt.Println(err)
		return
	}

	for result.Next() {
		var snsr TempSensorInfo
		err := result.Scan(&snsr.SnsrNo, &snsr.SnsrType, &snsr.SnsrMnemonic, &snsr.LowerLimit, &snsr.UpperLimit, &snsr.SnsrRef, &snsr.Description)
		if err != nil {
			fmt.Println(err)
		}
		snsr.Value = "No TM Data"
		TempSensorInfoList = append(TempSensorInfoList, snsr)
	}
}

// GetTempSensorsMnemonics : Returns Temp Sensors Mnemonics
func GetTempSensorsMnemonics() []string {
	var tempSensors []string

	for _, snsr := range TempSensorInfoList {
		tempSensors = append(tempSensors, snsr.SnsrMnemonic)
	}

	return tempSensors
}
