package db

import (
	"Autobots/dbcon"
	"fmt"
	"strconv"
)

// HeaterGroupInfo ...Structure for hw_htr_grp_info
type HeaterGroupInfo struct {
	GrpNo        string `json:"grp_no"`
	HtrGrpEnaCmd string `json:"ena_cmd"`
	HtrGrpDisCmd string `json:"dis_cmd"`
	HtrGrpSts    string `json:"grp_sts"`
	Description  string `json:"desc"`
}

//HeaterGroupInfoList : List of Heater Group Infos
var HeaterGroupInfoList []HeaterGroupInfo

// UpdateHeaterGroupCmdTM : Creates SQL table for Htr Grps
func UpdateHeaterGroupCmdTM() {
	var err error
	for i := 1; i <= 10; i++ {
		grpNo := strconv.Itoa(i)
		htrGrpEnaCmd := "Htr-Grp-" + strconv.Itoa(i) + "-Ena"
		htrGrpDisCmd := "Htr-Grp-" + strconv.Itoa(i) + "-Dis"
		htrGrpSts := "Htr-Grp-" + strconv.Itoa(i) + "-Sts"
		description := "Heater Group - " + strconv.Itoa(i)

		_, err = dbcon.DBObject.Exec("INSERT INTO `hw_htr_grp_info` (`grp_no`, `htr_grp_ena_cmd`, `htr_grp_dis_cmd`, `htr_grp_sts`, `description`)" +
			"VALUES (" + grpNo + ", '" + htrGrpEnaCmd + "', '" + htrGrpDisCmd + "', '" + htrGrpSts + "', '" + description + "')")
	}

	if err == nil {
		fmt.Println("hw_htr_grp_info table updated")
	}
}

// GetHeaterGroupsInfo : Gets Info from database
func GetHeaterGroupsInfo() {
	result, err := dbcon.DBObject.Query("SELECT grp_no, htr_grp_ena_cmd, htr_grp_dis_cmd, htr_grp_sts, description FROM `hw_htr_grp_info`")
	if err != nil {
		fmt.Println(err)
		return
	}

	for result.Next() {
		var htrGrp HeaterGroupInfo
		err := result.Scan(&htrGrp.GrpNo, &htrGrp.HtrGrpEnaCmd, &htrGrp.HtrGrpDisCmd, &htrGrp.HtrGrpSts, &htrGrp.Description)
		if err != nil {
			fmt.Println(err)
		}

		cmdQuery, _ := dbcon.DBObject.Query("SELECT `CommandCode` FROM `tc_oldtypes` WHERE `Mnemonic` = '" + htrGrp.HtrGrpEnaCmd + "'")
		if cmdQuery.Next() {
			cmdQuery.Scan(&htrGrp.HtrGrpEnaCmd)
		}
		cmdQuery.Close()

		cmdQuery, _ = dbcon.DBObject.Query("SELECT `CommandCode` FROM `tc_oldtypes` WHERE `Mnemonic` = '" + htrGrp.HtrGrpDisCmd + "'")
		if cmdQuery.Next() {
			cmdQuery.Scan(&htrGrp.HtrGrpDisCmd)
		}
		cmdQuery.Close()

		HeaterGroupInfoList = append(HeaterGroupInfoList, htrGrp)
	}
}

// GetHeaterGroupMnemonics : Returns Heater Group Mnemonics
func GetHeaterGroupMnemonics() []string {
	var heaterGrps []string

	for _, grp := range HeaterGroupInfoList {
		heaterGrps = append(heaterGrps, grp.HtrGrpSts)
	}

	return heaterGrps
}
