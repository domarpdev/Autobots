package main

import (
	"Autobots/dbcon"
	"Autobots/heatwave/websocket"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"

	scos "Autobots/jsonpubs/scos/SCOS_ADC_Publisher"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	ws "github.com/gorilla/websocket"

	zmq "github.com/pebbe/zmq4"
)

var smonParamMap map[string]string
var adcParamMap map[string]string
var smonClients []*ws.Conn
var adcClients []*ws.Conn

type scosParam struct {
	pid      string
	mnemonic string
}

type scosPkt struct {
	ParamList []scosParamInfo `json:"params"`
	Stream    string          `json:"stream"`
	SeqCount  string          `json:"seqcount"`
	Time      string          `json:"time_stamp"`
	Error     string          `json:"err_desc"`
}

type scosParamInfo struct {
	Mnemonic string `json:"param"`
	Value    string `json:"value"`
}

var smonAddr, adcAddr, wsSMON, wsADC *string

func main() {
	smonAddr = flag.String("smonaddr", "127.0.0.1:9012", "SCOS Publisher Address")
	adcAddr = flag.String("adcaddr", "127.0.0.1:9013", "ADC Publisher Address")
	wsSMON = flag.String("wssmon", "9060", "Websocket SCOS Publish Port")
	wsADC = flag.String("wsadc", "9061", "Websocket ADC Publish Port")

	flag.Parse()

	dbcon.ConnectToDB()
	getSMONMnemonics()
	getADCMnemonics()

	go startSMONPublisher()
	go startADCPublisher()

	go func() {
		r1 := mux.NewRouter()
		r1.HandleFunc("/", homePage)
		r1.HandleFunc("/ws", wsSMONEndPoint)

		log.Fatal(http.ListenAndServe(":"+*wsSMON, r1))
	}()

	r2 := mux.NewRouter()
	r2.HandleFunc("/", homePage)
	r2.HandleFunc("/ws", wsADCEndPoint)
	log.Fatal(http.ListenAndServe(":"+*wsADC, r2))
}

func getSMONMnemonics() {
	result, err := dbcon.DBObject.Query("SELECT `PID`, `Mnemonic` FROM `scos_output_read`")
	if err != nil {
		fmt.Println(err)
		return
	}

	var param scosParam
	smonParamMap = make(map[string]string)

	for result.Next() {
		err := result.Scan(&param.pid, &param.mnemonic)
		if err != nil {
			fmt.Println(err)
		}

		smonParamMap[param.pid] = param.mnemonic
	}
}

func getADCMnemonics() {
	result, err := dbcon.DBObject.Query("SELECT `PID`, `ParameterName` FROM `ADC_Table`")
	if err != nil {
		fmt.Println(err)
		return
	}

	var param scosParam
	adcParamMap = make(map[string]string)

	for result.Next() {
		err := result.Scan(&param.pid, &param.mnemonic)
		if err != nil {
			fmt.Println(err)
		}

		adcParamMap[param.pid] = param.mnemonic
	}
}

func startSMONPublisher() {
	protoPkt := &scos.ProcessedParameter{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *smonAddr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {

		jsonPkt := scosPkt{}
		pktParams := []scosParamInfo{}

		contents, err := subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		paramCount := len(protoPkt.GetPid())

		for i := 0; i < paramCount; i++ {
			pktParam := scosParamInfo{}

			pktParam.Mnemonic = smonParamMap[protoPkt.GetPid()[i]]
			pktParam.Value = protoPkt.GetProcValue()[i]

			pktParams = append(pktParams, pktParam)
		}

		jsonPkt.ParamList = pktParams
		jsonPkt.Stream = protoPkt.GetHwStreamId()
		jsonPkt.SeqCount = strconv.Itoa(int(protoPkt.GetMsgSeqCount()))
		jsonPkt.Time = protoPkt.GetTimestamp()
		jsonPkt.Error = protoPkt.GetErrorDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)

		isPrblm := false
		for _, client := range smonClients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}

		}

		if isPrblm == true {
			removeSMONClient()
		}
	}
}

func startADCPublisher() {
	protoPkt := &scos.ProcessedParameter{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *adcAddr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {

		jsonPkt := scosPkt{}
		pktParams := []scosParamInfo{}

		contents, _ := subscriber.RecvBytes(0)

		proto.Unmarshal(contents, protoPkt)

		paramCount := len(protoPkt.GetPid())

		for i := 0; i < paramCount; i++ {
			pktParam := scosParamInfo{}

			pktParam.Mnemonic = adcParamMap[protoPkt.GetPid()[i]]
			pktParam.Value = protoPkt.GetProcValue()[i]

			pktParams = append(pktParams, pktParam)
		}

		jsonPkt.ParamList = pktParams
		jsonPkt.Stream = protoPkt.GetHwStreamId()
		jsonPkt.SeqCount = strconv.Itoa(int(protoPkt.GetMsgSeqCount()))
		jsonPkt.Time = protoPkt.GetTimestamp()
		jsonPkt.Error = protoPkt.GetErrorDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)

		isPrblm := false
		for _, client := range adcClients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}

		}

		if isPrblm == true {
			removeADCClient()
		}
	}
}

func homePage(w http.ResponseWriter, r *http.Request) {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Fprintf(w, "Nothing Here! Go back %+v", ip)
}

func wsSMONEndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	smonClients = append(smonClients, conn)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New SMON client connected from", ip)
	fmt.Println("No. of SMON Clients: ", len(smonClients))
}

func wsADCEndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	adcClients = append(adcClients, conn)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New ADC client connected from", ip)
	fmt.Println("No. of ADC Clients: ", len(adcClients))
}

func removeSMONClient() {

	for index, client := range smonClients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			smonClients = append(smonClients[:index], smonClients[index+1:]...)
			removeSMONClient()
			break
		}
	}

	fmt.Println("SMON Client Disconnected")
	fmt.Println("No. of SMON Clients:", len(smonClients))
}

func removeADCClient() {

	for index, client := range adcClients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			adcClients = append(adcClients[:index], adcClients[index+1:]...)
			removeADCClient()
			break
		}
	}

	fmt.Println("ADC Client Disconnected")
	fmt.Println("No. of ADC Clients:", len(adcClients))
}
