/*
 * File         : lodash.go
 * Project      : Autobots
 * Created Date : Wednesday, Mar 4th 2020, 6:09:50 PM
 * Author       : Pramod Devireddy
 *
 * Last Modified:
 * Modified By  :
 *
 * Copyright (c)2020 - IMACS SCG-URSC/ISRO
 * ************************* Description *************************
 *
 * ***************************************************************
 */

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math"
	"net"
	"net/http"
	"runtime"
	"strconv"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"

	"Autobots/dbcon"
	"Autobots/heatwave/websocket"
	scos "Autobots/jsonpubs/scos/SCOS_ADC_Publisher"
	tm "Autobots/protos/ProcessedData"

	ws "github.com/gorilla/websocket"

	zmq "github.com/pebbe/zmq4"
)

type eventInfo struct {
	Event     string  `json:"event"`
	EventID   int64   `json:"event_id"`
	EventTime string  `json:"event_time"`
	LoadCur   float64 `json:"load_cur"`
	DeltaCur  float64 `json:"delta_cur"`
}

var eventList []eventInfo

var initLoadCur, finalLoadcur, curAuxRegVal, auxReg1, auxReg2, auxReg1PID, auxReg2PID, basLoadCurPID string

var basMLoadCur, basRLoadCur float64

var eventCount int64

var subscriber1, subscriber2 *zmq.Socket

var tcAddr, tmAddr, scos1Addr, scos2Addr *string

var eventMap map[string]string

var clients []*ws.Conn

func main() {
	runtime.GOMAXPROCS(4)

	tmAddr = flag.String("tmaddr", "127.0.0.1:5000", "TM Publisher Address")
	scos1Addr = flag.String("scos1addr", "127.0.0.1:9012", "SCOS1 Publisher Address")
	scos2Addr = flag.String("scos2addr", "127.0.0.1:9014", "SCOS2 Publisher Address")

	flag.Parse()

	eventCount = 0

	dbcon.ConnectToDB()
	getPIDs()
	loadEvents()

	go telemetrySubscription()
	go scos1Subscription()
	go scos2Subscription()

	// Initializing Router
	r := mux.NewRouter()
	r.HandleFunc("/event_list", getEventList)
	r.HandleFunc("/ws", wsEndpoint)
	log.Fatal(http.ListenAndServe(":9022", r))
}

func getPIDs() {

	result, _ := dbcon.DBObject.Query("SELECT `PID` FROM `tm_parameters` WHERE GCMnemonic = 'D1-Aux_register'")
	if result.Next() {
		result.Scan(&auxReg1PID)
	}

	result, _ = dbcon.DBObject.Query("SELECT `PID` FROM `tm_parameters` WHERE GCMnemonic = 'D2-Aux_register'")
	if result.Next() {
		result.Scan(&auxReg2PID)
	}

	result, _ = dbcon.DBObject.Query("SELECT `PID` FROM `scos_output_read` WHERE `Mnemonic` = 'BAS-LOAD-CUR'")
	if result.Next() {
		result.Scan(&basLoadCurPID)
	}

	result.Close()

	fmt.Println(auxReg1PID, auxReg2PID, basLoadCurPID)
}

func loadEvents() {
	result, _ := dbcon.DBObject.Query("SELECT `event`, `event_cmd` FROM `ld_events_info`")

	eventMap = make(map[string]string)
	var event, eventCmd string
	for result.Next() {
		result.Scan(&event, &eventCmd)
		eventMap[strings.ToUpper(eventCmd)] = event
	}

	result.Close()
}

func loadDB() {
	result, _ := dbcon.DBObject.Query("SELECT `Mnemonic`, `CommandCode` FROM `tc_oldtypes` WHERE `Mnemonic` LIKE '%of%'")

	var mnemonic, cmdCode string
	for result.Next() {
		result.Scan(&mnemonic, &cmdCode)

		fmt.Println(mnemonic, cmdCode)
		dbcon.DBObject.Exec("INSERT INTO `ld_events_info` (`event`, `event_cmd`) VALUES ('" + mnemonic + "', '" + cmdCode + "')")
	}

	result.Close()
}

func telemetrySubscription() {
	protoPkt := &tm.HKPacket{}

	subscriber1, _ = zmq.NewSocket(zmq.SUB)
	defer subscriber1.Close()
	subscriber1.Connect("tcp://" + *tmAddr)

	subscribeParam(1, auxReg1PID)
	subscribeParam(1, auxReg2PID)

	for {

		contents, err := subscriber1.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		if strings.ToUpper(protoPkt.GetParamID()) == strings.ToUpper(auxReg1PID) {
			AuxRegVal := strings.ToUpper(strings.TrimSpace(protoPkt.GetProcValue()))

			if AuxRegVal != auxReg1 {
				auxReg1 = AuxRegVal
				curAuxRegVal = AuxRegVal[2:]
			}
		} else if strings.ToUpper(protoPkt.GetParamID()) == strings.ToUpper(auxReg2PID) {
			AuxRegVal := strings.ToUpper(strings.TrimSpace(protoPkt.GetProcValue()))

			if AuxRegVal != auxReg2 {
				auxReg2 = AuxRegVal
				curAuxRegVal = AuxRegVal[2:]
			}
		}
	}
}

func subscribeParam(chain int, paramID string) {

	filter := make([]byte, 2)

	filter[0] = 0x0A
	filter[1] = byte(len(paramID))
	filter = append(filter, paramID...)

	if chain == 1 {
		subscriber1.SetSubscribe(string(filter))
	} else if chain == 2 {
		subscriber2.SetSubscribe(string(filter))
	}
}

func scos1Subscription() {
	protoPkt := &scos.ProcessedParameter{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *scos1Addr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {
		contents, err := subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		paramCount := len(protoPkt.GetPid())

		for i := 0; i < paramCount; i++ {
			if basLoadCurPID == protoPkt.GetPid()[i] {
				presentLoadCur, _ := strconv.ParseFloat(protoPkt.GetProcValue()[i], 64)
				deltaCur := math.Round(math.Abs(presentLoadCur-basMLoadCur)*100) / 100

				if deltaCur > 0.015 {
					basMLoadCur = presentLoadCur

					if eventMap[curAuxRegVal] != "" {
						jsonPkt := eventInfo{}

						jsonPkt.Event = eventMap[curAuxRegVal]
						jsonPkt.EventID = eventCount
						jsonPkt.EventTime = protoPkt.GetTimestamp()
						jsonPkt.LoadCur = basMLoadCur
						jsonPkt.DeltaCur = deltaCur

						eventList = append([]eventInfo{jsonPkt}, eventList...)

						fmtJSON, _ := json.Marshal(jsonPkt)
						writeMessageToClients(fmtJSON)

						eventCount++
					}

				}
			}
		}
	}
}

func scos2Subscription() {
	protoPkt := &scos.ProcessedParameter{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *scos2Addr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {
		contents, err := subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		paramCount := len(protoPkt.GetPid())

		for i := 0; i < paramCount; i++ {
			if basLoadCurPID == protoPkt.GetPid()[i] {
				presentLoadCur, _ := strconv.ParseFloat(protoPkt.GetProcValue()[i], 64)
				deltaCur := math.Round((basRLoadCur-presentLoadCur)*100) / 100

				basRLoadCur = presentLoadCur
				if deltaCur > 1 {

					jsonPkt := eventInfo{}

					jsonPkt.Event = "S/C ON"
					jsonPkt.EventID = eventCount
					jsonPkt.EventTime = protoPkt.GetTimestamp()
					jsonPkt.LoadCur = basRLoadCur
					jsonPkt.DeltaCur = deltaCur

					eventList = append([]eventInfo{jsonPkt}, eventList...)

					fmtJSON, _ := json.Marshal(jsonPkt)
					writeMessageToClients(fmtJSON)

					eventCount++
				} else if deltaCur < 1 {

					jsonPkt := eventInfo{}

					jsonPkt.Event = "S/C OFF"
					jsonPkt.EventID = eventCount
					jsonPkt.EventTime = protoPkt.GetTimestamp()
					jsonPkt.LoadCur = basRLoadCur
					jsonPkt.DeltaCur = deltaCur

					eventList = append([]eventInfo{jsonPkt}, eventList...)

					fmtJSON, _ := json.Marshal(jsonPkt)
					writeMessageToClients(fmtJSON)

					eventCount++
				}
			}
		}
	}
}

func getEventList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(eventList)
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	clients = append(clients, conn)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New client connected from", ip)
	fmt.Println("No. of Clients: ", len(clients))
}

func spacecraftOnEvent() {

}

func spacecraftOffEvent() {

}

func writeMessageToClients(msg []byte) {
	isPrblm := false
	for _, client := range clients {
		err := client.WriteMessage(ws.TextMessage, msg)
		if err != nil {
			isPrblm = true
		}
	}

	if isPrblm == true {
		removeClient()
	}
}

func removeClient() {

	for index, client := range clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			clients = append(clients[:index], clients[index+1:]...)
			removeClient()
			break
		}
	}

	fmt.Println("Client Disconnected")
	fmt.Println("No. of Clients:", len(clients))
}
