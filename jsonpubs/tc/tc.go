package main

import (
	"Autobots/dbcon"
	"Autobots/heatwave/websocket"
	tc "Autobots/jsonpubs/tc/TeleCommand"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	ws "github.com/gorilla/websocket"
	zmq "github.com/pebbe/zmq4"
)

var cmdDB map[string]string
var stsMap map[int64]string
var clients []*ws.Conn

type tcRecord struct {
	CID      string
	Mnemonic string
}

type tcSts struct {
	stsCode int64
	stsDesc string
}

type tcPacket struct {
	Command  string `json:"cmd"`
	Code     string `json:"code"`
	FullCode string `json:"full_code"`
	DataPart string `json:"data_part"`
	Status   string `json:"status"`
	Time     string `json:"time"`
}

var wsPort, tcAddr *string

func main() {
	tcAddr = flag.String("tcaddr", "127.0.0.1:5020", "TC Publisher Address")
	wsPort = flag.String("wsport", "9070", "Websocket Publish Port")

	flag.Parse()

	dbcon.ConnectToDB()
	getTCMnemonics()
	getTCStsDescription()

	go startSubscribing()

	r := mux.NewRouter()
	r.HandleFunc("/", homePage)
	r.HandleFunc("/ws", wsEndpoint)
	log.Fatal(http.ListenAndServe(":"+*wsPort, r))
}

func getTCMnemonics() {
	result, err := dbcon.DBObject.Query("SELECT `CID`, `Mnemonic` FROM `tc_oldtypes`")
	if err != nil {
		fmt.Println(err)
		return
	}

	var tcRow tcRecord
	cmdDB = make(map[string]string)

	for result.Next() {
		err := result.Scan(&tcRow.CID, &tcRow.Mnemonic)
		if err != nil {
			fmt.Println(err)
		}

		cmdDB[tcRow.CID] = strings.TrimSpace(tcRow.Mnemonic)
	}
}

func getTCStsDescription() {
	result, err := dbcon.DBObject.Query("SELECT `CmdSts`, `CommandDescription` FROM `TCCmdSts`")
	if err != nil {
		fmt.Println(err)
		return
	}

	var stsRow tcSts
	stsMap = make(map[int64]string)

	for result.Next() {
		err := result.Scan(&stsRow.stsCode, &stsRow.stsDesc)
		if err != nil {
			fmt.Println(err)
		}

		stsMap[stsRow.stsCode] = stsRow.stsDesc
	}
}

func startSubscribing() {
	protoPkt := &tc.TCPacket{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *tcAddr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {

		jsonPkt := tcPacket{}

		contents, _ := subscriber.RecvBytes(0)

		proto.Unmarshal(contents, protoPkt)

		jsonPkt.Command = cmdDB[protoPkt.GetCommandID()]
		jsonPkt.Code = string(protoPkt.GetCommandCode())
		jsonPkt.FullCode = string(protoPkt.GetFullCode())
		jsonPkt.DataPart = protoPkt.GetDataPart()
		jsonPkt.Status = stsMap[protoPkt.GetCommandExecutionSts()]
		jsonPkt.Time = protoPkt.GetTimeOfCommand()

		fmtJSON, _ := json.Marshal(jsonPkt)

		isPrblm := false
		for _, client := range clients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}

		}

		if isPrblm == true {
			removeClient()
		}
	}
}

func homePage(w http.ResponseWriter, r *http.Request) {

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)

	fmt.Fprintf(w, "Nothing Here! Go back %+v", ip)
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	clients = append(clients, conn)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New client connected from", ip)
	fmt.Println("No. of Clients: ", len(clients))
}

func removeClient() {

	for index, client := range clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			clients = append(clients[:index], clients[index+1:]...)
			removeClient()
			break
		}
	}

	fmt.Println("Client Disconnected")
	fmt.Println("No. of Clients:", len(clients))
}
