// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ProcessedPublish.proto

package ProcessedData

import (
	fmt "fmt"

	proto "github.com/golang/protobuf/proto"

	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type HKPacket struct {
	ParamID              *string  `protobuf:"bytes,1,opt,name=paramID" json:"paramID,omitempty"`
	SourceInfo           *string  `protobuf:"bytes,2,opt,name=source_info,json=sourceInfo" json:"source_info,omitempty"`
	RawCount             *int64   `protobuf:"varint,3,opt,name=rawCount" json:"rawCount,omitempty"`
	ProcValue            *string  `protobuf:"bytes,4,opt,name=procValue" json:"procValue,omitempty"`
	TimeStamp            *string  `protobuf:"bytes,5,opt,name=timeStamp" json:"timeStamp,omitempty"`
	UpperLimit           *float64 `protobuf:"fixed64,6,opt,name=UpperLimit" json:"UpperLimit,omitempty"`
	LowerLimit           *float64 `protobuf:"fixed64,7,opt,name=LowerLimit" json:"LowerLimit,omitempty"`
	ErrorCode            *int64   `protobuf:"varint,8,opt,name=errorCode" json:"errorCode,omitempty"`
	ErrDescription       *string  `protobuf:"bytes,9,opt,name=errDescription" json:"errDescription,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HKPacket) Reset()         { *m = HKPacket{} }
func (m *HKPacket) String() string { return proto.CompactTextString(m) }
func (*HKPacket) ProtoMessage()    {}
func (*HKPacket) Descriptor() ([]byte, []int) {
	return fileDescriptor_ProcessedPublish_75957d3e44e929e2, []int{0}
}
func (m *HKPacket) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HKPacket.Unmarshal(m, b)
}
func (m *HKPacket) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HKPacket.Marshal(b, m, deterministic)
}
func (dst *HKPacket) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HKPacket.Merge(dst, src)
}
func (m *HKPacket) XXX_Size() int {
	return xxx_messageInfo_HKPacket.Size(m)
}
func (m *HKPacket) XXX_DiscardUnknown() {
	xxx_messageInfo_HKPacket.DiscardUnknown(m)
}

var xxx_messageInfo_HKPacket proto.InternalMessageInfo

func (m *HKPacket) GetParamID() string {
	if m != nil && m.ParamID != nil {
		return *m.ParamID
	}
	return ""
}

func (m *HKPacket) GetSourceInfo() string {
	if m != nil && m.SourceInfo != nil {
		return *m.SourceInfo
	}
	return ""
}

func (m *HKPacket) GetRawCount() int64 {
	if m != nil && m.RawCount != nil {
		return *m.RawCount
	}
	return 0
}

func (m *HKPacket) GetProcValue() string {
	if m != nil && m.ProcValue != nil {
		return *m.ProcValue
	}
	return ""
}

func (m *HKPacket) GetTimeStamp() string {
	if m != nil && m.TimeStamp != nil {
		return *m.TimeStamp
	}
	return ""
}

func (m *HKPacket) GetUpperLimit() float64 {
	if m != nil && m.UpperLimit != nil {
		return *m.UpperLimit
	}
	return 0
}

func (m *HKPacket) GetLowerLimit() float64 {
	if m != nil && m.LowerLimit != nil {
		return *m.LowerLimit
	}
	return 0
}

func (m *HKPacket) GetErrorCode() int64 {
	if m != nil && m.ErrorCode != nil {
		return *m.ErrorCode
	}
	return 0
}

func (m *HKPacket) GetErrDescription() string {
	if m != nil && m.ErrDescription != nil {
		return *m.ErrDescription
	}
	return ""
}

type PTMPacket struct {
	Mnemonic             *string  `protobuf:"bytes,1,opt,name=mnemonic" json:"mnemonic,omitempty"`
	SourceInfo           *string  `protobuf:"bytes,2,opt,name=source_info,json=sourceInfo" json:"source_info,omitempty"`
	RawCount             *int64   `protobuf:"varint,3,opt,name=rawCount" json:"rawCount,omitempty"`
	ProcValue            *string  `protobuf:"bytes,4,opt,name=procValue" json:"procValue,omitempty"`
	TimeStamp            *string  `protobuf:"bytes,5,opt,name=timeStamp" json:"timeStamp,omitempty"`
	UpperLimit           *float64 `protobuf:"fixed64,6,opt,name=UpperLimit" json:"UpperLimit,omitempty"`
	LowerLimit           *float64 `protobuf:"fixed64,7,opt,name=LowerLimit" json:"LowerLimit,omitempty"`
	ErrorCode            *int64   `protobuf:"varint,8,opt,name=errorCode" json:"errorCode,omitempty"`
	ErrDescription       *string  `protobuf:"bytes,9,opt,name=errDescription" json:"errDescription,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PTMPacket) Reset()         { *m = PTMPacket{} }
func (m *PTMPacket) String() string { return proto.CompactTextString(m) }
func (*PTMPacket) ProtoMessage()    {}
func (*PTMPacket) Descriptor() ([]byte, []int) {
	return fileDescriptor_ProcessedPublish_75957d3e44e929e2, []int{1}
}
func (m *PTMPacket) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PTMPacket.Unmarshal(m, b)
}
func (m *PTMPacket) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PTMPacket.Marshal(b, m, deterministic)
}
func (dst *PTMPacket) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PTMPacket.Merge(dst, src)
}
func (m *PTMPacket) XXX_Size() int {
	return xxx_messageInfo_PTMPacket.Size(m)
}
func (m *PTMPacket) XXX_DiscardUnknown() {
	xxx_messageInfo_PTMPacket.DiscardUnknown(m)
}

var xxx_messageInfo_PTMPacket proto.InternalMessageInfo

func (m *PTMPacket) GetMnemonic() string {
	if m != nil && m.Mnemonic != nil {
		return *m.Mnemonic
	}
	return ""
}

func (m *PTMPacket) GetSourceInfo() string {
	if m != nil && m.SourceInfo != nil {
		return *m.SourceInfo
	}
	return ""
}

func (m *PTMPacket) GetRawCount() int64 {
	if m != nil && m.RawCount != nil {
		return *m.RawCount
	}
	return 0
}

func (m *PTMPacket) GetProcValue() string {
	if m != nil && m.ProcValue != nil {
		return *m.ProcValue
	}
	return ""
}

func (m *PTMPacket) GetTimeStamp() string {
	if m != nil && m.TimeStamp != nil {
		return *m.TimeStamp
	}
	return ""
}

func (m *PTMPacket) GetUpperLimit() float64 {
	if m != nil && m.UpperLimit != nil {
		return *m.UpperLimit
	}
	return 0
}

func (m *PTMPacket) GetLowerLimit() float64 {
	if m != nil && m.LowerLimit != nil {
		return *m.LowerLimit
	}
	return 0
}

func (m *PTMPacket) GetErrorCode() int64 {
	if m != nil && m.ErrorCode != nil {
		return *m.ErrorCode
	}
	return 0
}

func (m *PTMPacket) GetErrDescription() string {
	if m != nil && m.ErrDescription != nil {
		return *m.ErrDescription
	}
	return ""
}

type DwellPacket struct {
	Parameters           *DwellPacket_ParamInfo `protobuf:"bytes,1,opt,name=parameters" json:"parameters,omitempty"`
	SourceInfo           *string                `protobuf:"bytes,2,opt,name=source_info,json=sourceInfo" json:"source_info,omitempty"`
	TimeStamp            *string                `protobuf:"bytes,3,opt,name=timeStamp" json:"timeStamp,omitempty"`
	ErrorCode            *int64                 `protobuf:"varint,4,opt,name=errorCode" json:"errorCode,omitempty"`
	XXX_NoUnkeyedLiteral struct{}               `json:"-"`
	XXX_unrecognized     []byte                 `json:"-"`
	XXX_sizecache        int32                  `json:"-"`
}

func (m *DwellPacket) Reset()         { *m = DwellPacket{} }
func (m *DwellPacket) String() string { return proto.CompactTextString(m) }
func (*DwellPacket) ProtoMessage()    {}
func (*DwellPacket) Descriptor() ([]byte, []int) {
	return fileDescriptor_ProcessedPublish_75957d3e44e929e2, []int{2}
}
func (m *DwellPacket) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DwellPacket.Unmarshal(m, b)
}
func (m *DwellPacket) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DwellPacket.Marshal(b, m, deterministic)
}
func (dst *DwellPacket) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DwellPacket.Merge(dst, src)
}
func (m *DwellPacket) XXX_Size() int {
	return xxx_messageInfo_DwellPacket.Size(m)
}
func (m *DwellPacket) XXX_DiscardUnknown() {
	xxx_messageInfo_DwellPacket.DiscardUnknown(m)
}

var xxx_messageInfo_DwellPacket proto.InternalMessageInfo

func (m *DwellPacket) GetParameters() *DwellPacket_ParamInfo {
	if m != nil {
		return m.Parameters
	}
	return nil
}

func (m *DwellPacket) GetSourceInfo() string {
	if m != nil && m.SourceInfo != nil {
		return *m.SourceInfo
	}
	return ""
}

func (m *DwellPacket) GetTimeStamp() string {
	if m != nil && m.TimeStamp != nil {
		return *m.TimeStamp
	}
	return ""
}

func (m *DwellPacket) GetErrorCode() int64 {
	if m != nil && m.ErrorCode != nil {
		return *m.ErrorCode
	}
	return 0
}

type DwellPacket_ParamInfo struct {
	Mnemonic             *string  `protobuf:"bytes,1,opt,name=mnemonic" json:"mnemonic,omitempty"`
	RawCount             *int64   `protobuf:"varint,2,opt,name=rawCount" json:"rawCount,omitempty"`
	ProcValue            *string  `protobuf:"bytes,3,opt,name=procValue" json:"procValue,omitempty"`
	UpperLimit           *float64 `protobuf:"fixed64,4,opt,name=UpperLimit" json:"UpperLimit,omitempty"`
	LowerLimit           *float64 `protobuf:"fixed64,5,opt,name=LowerLimit" json:"LowerLimit,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DwellPacket_ParamInfo) Reset()         { *m = DwellPacket_ParamInfo{} }
func (m *DwellPacket_ParamInfo) String() string { return proto.CompactTextString(m) }
func (*DwellPacket_ParamInfo) ProtoMessage()    {}
func (*DwellPacket_ParamInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_ProcessedPublish_75957d3e44e929e2, []int{2, 0}
}
func (m *DwellPacket_ParamInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DwellPacket_ParamInfo.Unmarshal(m, b)
}
func (m *DwellPacket_ParamInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DwellPacket_ParamInfo.Marshal(b, m, deterministic)
}
func (dst *DwellPacket_ParamInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DwellPacket_ParamInfo.Merge(dst, src)
}
func (m *DwellPacket_ParamInfo) XXX_Size() int {
	return xxx_messageInfo_DwellPacket_ParamInfo.Size(m)
}
func (m *DwellPacket_ParamInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_DwellPacket_ParamInfo.DiscardUnknown(m)
}

var xxx_messageInfo_DwellPacket_ParamInfo proto.InternalMessageInfo

func (m *DwellPacket_ParamInfo) GetMnemonic() string {
	if m != nil && m.Mnemonic != nil {
		return *m.Mnemonic
	}
	return ""
}

func (m *DwellPacket_ParamInfo) GetRawCount() int64 {
	if m != nil && m.RawCount != nil {
		return *m.RawCount
	}
	return 0
}

func (m *DwellPacket_ParamInfo) GetProcValue() string {
	if m != nil && m.ProcValue != nil {
		return *m.ProcValue
	}
	return ""
}

func (m *DwellPacket_ParamInfo) GetUpperLimit() float64 {
	if m != nil && m.UpperLimit != nil {
		return *m.UpperLimit
	}
	return 0
}

func (m *DwellPacket_ParamInfo) GetLowerLimit() float64 {
	if m != nil && m.LowerLimit != nil {
		return *m.LowerLimit
	}
	return 0
}

func init() {
	proto.RegisterType((*HKPacket)(nil), "ProcessedData.HKPacket")
	proto.RegisterType((*PTMPacket)(nil), "ProcessedData.PTMPacket")
	proto.RegisterType((*DwellPacket)(nil), "ProcessedData.DwellPacket")
	proto.RegisterType((*DwellPacket_ParamInfo)(nil), "ProcessedData.DwellPacket.ParamInfo")
}

func init() {
	proto.RegisterFile("ProcessedPublish.proto", fileDescriptor_ProcessedPublish_75957d3e44e929e2)
}

var fileDescriptor_ProcessedPublish_75957d3e44e929e2 = []byte{
	// 345 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe4, 0x92, 0xcf, 0x6a, 0xb3, 0x50,
	0x10, 0xc5, 0x51, 0x93, 0x2f, 0x3a, 0xe1, 0xeb, 0xc2, 0x45, 0xb9, 0x84, 0xd2, 0x86, 0x50, 0x4a,
	0x56, 0x2e, 0xfa, 0x0a, 0x71, 0xd1, 0xd0, 0x14, 0xc4, 0xfe, 0xd9, 0x96, 0x5b, 0x33, 0xa1, 0x97,
	0xaa, 0x23, 0xe3, 0x95, 0x3c, 0x4f, 0x29, 0xf4, 0x89, 0xfa, 0x40, 0x25, 0x37, 0x89, 0x51, 0x17,
	0xc9, 0x03, 0x74, 0x39, 0xe7, 0x38, 0xde, 0x99, 0xdf, 0x19, 0x38, 0x8f, 0x98, 0x12, 0x2c, 0x4b,
	0x5c, 0x46, 0xd5, 0x5b, 0xaa, 0xca, 0xf7, 0xa0, 0x60, 0xd2, 0xe4, 0xff, 0xaf, 0xf5, 0x50, 0x6a,
	0x39, 0xf9, 0xb2, 0xc1, 0xbd, 0xbb, 0x8f, 0x64, 0xf2, 0x81, 0xda, 0x17, 0x30, 0x28, 0x24, 0xcb,
	0x6c, 0x1e, 0x0a, 0x6b, 0x6c, 0x4d, 0xbd, 0x78, 0x5f, 0xfa, 0x57, 0x30, 0x2c, 0xa9, 0xe2, 0x04,
	0x5f, 0x55, 0xbe, 0x22, 0x61, 0x1b, 0x17, 0xb6, 0xd2, 0x3c, 0x5f, 0x91, 0x3f, 0x02, 0x97, 0xe5,
	0x7a, 0x46, 0x55, 0xae, 0x85, 0x33, 0xb6, 0xa6, 0x4e, 0x5c, 0xd7, 0xfe, 0x05, 0x78, 0x05, 0x53,
	0xf2, 0x22, 0xd3, 0x0a, 0x45, 0xcf, 0xb4, 0x1e, 0x84, 0x8d, 0xab, 0x55, 0x86, 0x8f, 0x5a, 0x66,
	0x85, 0xe8, 0x6f, 0xdd, 0x5a, 0xf0, 0x2f, 0x01, 0x9e, 0x8b, 0x02, 0x79, 0xa1, 0x32, 0xa5, 0xc5,
	0xbf, 0xb1, 0x35, 0xb5, 0xe2, 0x86, 0xb2, 0xf1, 0x17, 0xb4, 0xde, 0xfb, 0x83, 0xad, 0x7f, 0x50,
	0x36, 0x7f, 0x47, 0x66, 0xe2, 0x19, 0x2d, 0x51, 0xb8, 0x66, 0xb0, 0x83, 0xe0, 0xdf, 0xc0, 0x19,
	0x32, 0x87, 0x58, 0x26, 0xac, 0x0a, 0xad, 0x28, 0x17, 0x9e, 0x19, 0xa0, 0xa3, 0x4e, 0xbe, 0x6d,
	0xf0, 0xa2, 0xa7, 0x87, 0x1d, 0xa6, 0x11, 0xb8, 0x59, 0x8e, 0x19, 0xe5, 0x2a, 0xd9, 0x71, 0xaa,
	0xeb, 0xbf, 0x0e, 0xea, 0xc7, 0x86, 0x61, 0xb8, 0xc6, 0x34, 0xdd, 0xa1, 0x0a, 0x01, 0xcc, 0x09,
	0xa1, 0x46, 0x2e, 0x0d, 0xac, 0xe1, 0xed, 0x75, 0xd0, 0x3a, 0xc1, 0xa0, 0xf1, 0x7d, 0x10, 0x99,
	0x7b, 0xcb, 0x57, 0x14, 0x37, 0xfa, 0x4e, 0x43, 0x6d, 0xa1, 0x71, 0xba, 0x68, 0x5a, 0xab, 0xf5,
	0x3a, 0xab, 0x8d, 0x3e, 0x2d, 0xf0, 0xea, 0x67, 0x8f, 0x66, 0xdb, 0x8c, 0xce, 0x3e, 0x16, 0x9d,
	0xd3, 0x8d, 0xae, 0x1d, 0x4e, 0xef, 0x44, 0x38, 0xfd, 0x6e, 0x38, 0xbf, 0x01, 0x00, 0x00, 0xff,
	0xff, 0x7e, 0xad, 0x45, 0xe3, 0xcd, 0x03, 0x00, 0x00,
}
