/*
 * File         : heatwave.go
 * Project      : Autobots
 * Created Date : Wednesday, Feb 19th 2020, 12:15:32 PM
 * Author       : Pramod Devireddy
 *
 * Last Modified:
 * Modified By  :
 *
 * Copyright (c)2020 - IMACS SCG-URSC/ISRO
 * ************************* Description *************************
 *  Main file for Heat Wave server.
 *  API Port: 9020
 * ***************************************************************
 */

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"runtime"
	"strings"

	"Autobots/dbcon"
	"Autobots/heatwave/db"
	"Autobots/heatwave/websocket"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"

	scos "Autobots/jsonpubs/scos/SCOS_ADC_Publisher"
	tc "Autobots/jsonpubs/tc/TeleCommand"
	tm "Autobots/protos/ProcessedData"

	ws "github.com/gorilla/websocket"

	zmq "github.com/pebbe/zmq4"
)

type heaterInfo struct {
	HeaterNo     string `json:"htr_no"`
	HeaterName   string `json:"htr_name"`
	HeaterStatus string `json:"htr_sts"`
	GroupNo      string `json:"grp_no"`
	SensorName   string `json:"snsr_name"`
	SensorValue  string `json:"snsr_val"`
}

type heaterGroupInfo struct {
	GroupNo     string       `json:"grp"`
	GroupStatus string       `json:"grp_sts"`
	Heaters     []heaterInfo `json:"htrs"`
}

type heaterCurrentsInfo struct {
	HeaterName       string `json:"htr"`
	HeaterStatus     string `json:"status"`
	ExpPower         string `json:"exp_power"`
	BusVoltage       string `json:"bus_vol"`
	ExpCurrent       string `json:"exp_cur"`
	InitCurrent      string `json:"init_cur"`
	FinalCurrent     string `json:"final_cur"`
	HeaterCurrent    string `json:"htr_cur"`
	CurrentVariation string `json:"variation"`
	HeaterHealth     string `json:"health"`
}

type heaterGroupUpdate struct {
	Type              string `json:"type"`
	HeaterGroup       string `json:"htr_grp"`
	HeaterGroupStatus string `json:"htr_grp_sts"`
}

type heaterUpdate struct {
	Type         string `json:"type"`
	HeaterName   string `json:"htr_name"`
	HeaterStatus string `json:"htr_sts"`
}

type sensorUpdate struct {
	Type        string `json:"type"`
	SensorName  string `json:"snsr_name"`
	SensorValue string `json:"snsr_val"`
}

type heaterCurrentUpdate struct {
	Type             string `json:"type"`
	HeaterName       string `json:"htr_name"`
	InitCurrent      string `json:"init_cur"`
	FinalCurrent     string `json:"final_cur"`
	HeaterCurrent    string `json:"htr_cur"`
	CurrentVariation string `json:"cur_var"`
	CurrentStatus    string `json:"cur_sts"`
}

type heaterGroupCmdInfo struct {
	HeaterGroup       string
	HeaterGroupEnaCmd string
	HeaterGroupDisCmd string
}

type heaterCmdInfo struct {
	HeaterName   string
	HeaterONCmd  string
	HeaterOFFCmd string
}

var heaterList []heaterInfo
var heaterGroupList []heaterGroupInfo
var heaterCurList []heaterCurrentsInfo

var heaterCmdsList []heaterCmdInfo
var heaterGroupCmdsList []heaterGroupCmdInfo

var initLoadCur, finalLoadcur, auxReg1, auxReg2, auxReg1PID, auxReg2PID, basLoadCur, basLoadCurPID string

var tmMnemonicList map[string]string

var subscriber1, subscriber2 *zmq.Socket

var tcAddr, tmAddr, scosAddr *string

var clients []*ws.Conn

func main() {
	runtime.GOMAXPROCS(4)

	tmAddr = flag.String("tmaddr", "127.0.0.1:5000", "SCOS Publisher Address")
	scosAddr = flag.String("scosaddr", "127.0.0.1:9012", "SCOS Publisher Address")
	tcAddr = flag.String("tcaddr", "127.0.0.1:9070", "SCOS Publisher Address")

	flag.Parse()

	dbcon.ConnectToDB()
	// db.UpdateTempSensorTM()
	db.GetHeatersInfo()
	db.GetHeaterGroupsInfo()
	db.GetTempSensorsInfo()

	tmMnemonicList = make(map[string]string)
	loadHeaterList()
	loadHeaterGroupList()
	loadTempSensorList()
	loadHeatersCurrentList()
	getPIDs()

	go telemetrySubscription()
	go telecommandSubscription()
	go scosSubscription()

	// Initializing Router
	r := mux.NewRouter()
	setupRoutes(r)
	log.Fatal(http.ListenAndServe(":9020", r))

	fmt.Println("Heat Wave Signing Off...")
}

func setupRoutes(r *mux.Router) {
	// Route Handlers & Endpoints
	r.HandleFunc("/htr_grp_list", getHeaterGroupsList)
	r.HandleFunc("/htr_list", getHeatersList)
	r.HandleFunc("/tmp_sensor_list", getSensorsList)
	r.HandleFunc("/htr_cur_list", getHtrsCurList)

	r.HandleFunc("/ws", wsEndpoint)
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	clients = append(clients, conn)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New client connected from", ip)
	fmt.Println("No. of Clients: ", len(clients))
}

func getHeaterGroupsList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(heaterGroupList)
}

func getHeatersList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(heaterList)
}

func getSensorsList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(db.TempSensorInfoList)
}

func getHtrsCurList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(heaterCurList)
}

func getPIDs() {

	result, _ := dbcon.DBObject.Query("SELECT `PID` FROM `tm_parameters` WHERE GCMnemonic = 'D1-Aux_register'")
	if result.Next() {
		result.Scan(&auxReg1PID)
	}

	result, _ = dbcon.DBObject.Query("SELECT `PID` FROM `tm_parameters` WHERE GCMnemonic = 'D2-Aux_register'")
	if result.Next() {
		result.Scan(&auxReg2PID)
	}

	result, _ = dbcon.DBObject.Query("SELECT `PID` FROM `scos_output_read` WHERE `Mnemonic` = 'BAS-LOAD-CUR'")
	if result.Next() {
		result.Scan(&basLoadCurPID)
	}
}

func loadHeaterList() {
	for _, htrInfo := range db.HeaterInfoList {
		var htr heaterInfo
		var htrCmds heaterCmdInfo

		htr.HeaterNo = htrInfo.HtrNo
		htr.HeaterName = htrInfo.HtrName
		htr.HeaterStatus = "off"
		htr.GroupNo = htrInfo.GrpNo
		htr.SensorName = htrInfo.SnsrName
		htr.SensorValue = ""

		heaterList = append(heaterList, htr)

		htrCmds.HeaterName = htrInfo.HtrName
		htrCmds.HeaterONCmd = htrInfo.HtrOnCmd
		htrCmds.HeaterOFFCmd = htrInfo.HtrOffCmd

		heaterCmdsList = append(heaterCmdsList, htrCmds)
	}

}

func loadHeaterGroupList() {
	for _, htrGrpInfo := range db.HeaterGroupInfoList {
		var htrGrp heaterGroupInfo
		var htrGrpCmds heaterGroupCmdInfo

		htrGrp.GroupNo = htrGrpInfo.GrpNo
		htrGrp.GroupStatus = "off"

		for _, htr := range heaterList {
			if htr.GroupNo == htrGrp.GroupNo {
				htrGrp.Heaters = append(htrGrp.Heaters, htr)
			}
		}

		heaterGroupList = append(heaterGroupList, htrGrp)

		htrGrpCmds.HeaterGroup = htrGrpInfo.GrpNo
		htrGrpCmds.HeaterGroupEnaCmd = htrGrpInfo.HtrGrpEnaCmd
		htrGrpCmds.HeaterGroupDisCmd = htrGrpInfo.HtrGrpDisCmd

		heaterGroupCmdsList = append(heaterGroupCmdsList, htrGrpCmds)
	}
}

func loadTempSensorList() {
	var snsrs = db.GetTempSensorsMnemonics()
	var pid string

	for _, mnemonic := range snsrs {
		result, err := dbcon.DBObject.Query("SELECT `PID` FROM `tm_parameters` WHERE GCMnemonic = '" + mnemonic + "'")
		if err != nil {
			fmt.Println(err)
			return
		}

		for result.Next() {
			err := result.Scan(&pid)
			if err != nil {
				fmt.Println(err)
			}

			tmMnemonicList[pid] = strings.TrimSpace(mnemonic)
		}
	}
}

func loadHeatersCurrentList() {
	for _, htrInfo := range db.HeaterInfoList {
		var htr heaterCurrentsInfo

		htr.HeaterName = htrInfo.HtrName
		heaterCurList = append(heaterCurList, htr)
	}
}

func telemetrySubscription() {
	protoPkt := &tm.HKPacket{}

	subscriber1, _ = zmq.NewSocket(zmq.SUB)
	defer subscriber1.Close()
	subscriber1.Connect("tcp://" + *tmAddr)

	subscribeParam(1, auxReg1PID)
	subscribeParam(1, auxReg2PID)

	for {

		contents, err := subscriber1.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		if strings.ToUpper(protoPkt.GetParamID()) == strings.ToUpper(auxReg1PID) {
			curAuxRegVal := strings.ToUpper(protoPkt.GetProcValue())

			if curAuxRegVal != auxReg1 {
				if codeValid(curAuxRegVal) {
					auxReg1 = curAuxRegVal
					matchAndUpdate(curAuxRegVal)
				}
			}
		} else if strings.ToUpper(protoPkt.GetParamID()) == strings.ToUpper(auxReg2PID) {
			curAuxRegVal := strings.ToUpper(protoPkt.GetProcValue())

			if curAuxRegVal != auxReg2 {
				if codeValid(curAuxRegVal) {
					auxReg2 = curAuxRegVal
					matchAndUpdate(curAuxRegVal)
				}
			}
		}
	}
}

func codeValid(auxVal string) bool {
	for _, elem := range heaterCmdsList {
		if strings.Contains(auxVal, elem.HeaterONCmd) || strings.Contains(auxVal, elem.HeaterOFFCmd) {
			return true
		}
	}

	for _, elem := range heaterGroupCmdsList {
		if strings.Contains(auxVal, elem.HeaterGroupEnaCmd) || strings.Contains(auxVal, elem.HeaterGroupDisCmd) {
			return true
		}
	}

	return false
}

func subscribeParam(chain int, paramID string) {

	filter := make([]byte, 2)

	filter[0] = 0x0A
	filter[1] = byte(len(paramID))
	filter = append(filter, paramID...)

	if chain == 1 {
		subscriber1.SetSubscribe(string(filter))
	} else if chain == 2 {
		subscriber2.SetSubscribe(string(filter))
	}
}

func mapkey(m map[string]string, value string) (key string, ok bool) {
	for k, v := range m {
		if v == value {
			key = k
			ok = true
			return
		}
	}
	return
}

func scosSubscription() {
	protoPkt := &scos.ProcessedParameter{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *scosAddr)

	filter := ""

	subscriber.SetSubscribe(filter)

	for {
		contents, err := subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		paramCount := len(protoPkt.GetPid())

		for i := 0; i < paramCount; i++ {
			if basLoadCurPID == protoPkt.GetPid()[i] {
				basLoadCur = protoPkt.GetProcValue()[i]
			}
		}
	}
}

func telecommandSubscription() {
	protoPkt := &tc.TCPacket{}

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()
	subscriber.Connect("tcp://" + *tcAddr)

	filter := ""
	subscriber.SetSubscribe(filter)

	for {
		contents, _ := subscriber.RecvBytes(0)
		proto.Unmarshal(contents, protoPkt)
	}
}

func matchAndUpdate(cmdCode string) {
	for _, elem := range heaterGroupCmdsList {
		if strings.Contains(strings.ToUpper(cmdCode), strings.ToUpper(elem.HeaterGroupEnaCmd)) {
			jsonPkt := heaterGroupUpdate{}

			jsonPkt.Type = "htr_grp"
			jsonPkt.HeaterGroup = elem.HeaterGroup
			jsonPkt.HeaterGroupStatus = "on"

			fmtJSON, _ := json.Marshal(jsonPkt)
			writeMessageToClients(fmtJSON)

			for index, htrGrp := range heaterGroupList {
				if htrGrp.GroupNo == elem.HeaterGroup {
					heaterGroupList[index].GroupStatus = "on"
					break
				}
			}
			return
		} else if strings.Contains(strings.ToUpper(cmdCode), strings.ToUpper(elem.HeaterGroupDisCmd)) {
			jsonPkt := heaterGroupUpdate{}

			jsonPkt.Type = "htr_grp"
			jsonPkt.HeaterGroup = elem.HeaterGroup
			jsonPkt.HeaterGroupStatus = "off"

			fmtJSON, _ := json.Marshal(jsonPkt)
			writeMessageToClients(fmtJSON)

			for index, htrGrp := range heaterGroupList {
				if htrGrp.GroupNo == elem.HeaterGroup {
					heaterGroupList[index].GroupStatus = "off"
					break
				}
			}
			return
		}
	}

	for _, elem := range heaterCmdsList {
		if strings.Contains(strings.ToUpper(cmdCode), strings.ToUpper(elem.HeaterONCmd)) {
			jsonPkt := heaterUpdate{}

			jsonPkt.Type = "htr"
			jsonPkt.HeaterName = elem.HeaterName
			jsonPkt.HeaterStatus = "on"

			fmtJSON, _ := json.Marshal(jsonPkt)
			writeMessageToClients(fmtJSON)

			for index, htr := range heaterList {
				if htr.HeaterName == elem.HeaterName {
					heaterList[index].HeaterStatus = "on"

					for grpID, htrGrp := range heaterGroupList {
						if htrGrp.GroupNo == htr.GroupNo {
							for htrID, grpHtr := range htrGrp.Heaters {
								if grpHtr.HeaterName == elem.HeaterName {
									heaterGroupList[grpID].Heaters[htrID].HeaterStatus = "on"
									break
								}
							}
							break
						}
					}
					break
				}
			}
			return
		} else if strings.Contains(strings.ToUpper(cmdCode), strings.ToUpper(elem.HeaterOFFCmd)) {
			jsonPkt := heaterUpdate{}

			jsonPkt.Type = "htr"
			jsonPkt.HeaterName = elem.HeaterName
			jsonPkt.HeaterStatus = "off"

			fmtJSON, _ := json.Marshal(jsonPkt)
			writeMessageToClients(fmtJSON)

			for index, htr := range heaterList {
				if htr.HeaterName == elem.HeaterName {
					heaterList[index].HeaterStatus = "off"

					for grpID, htrGrp := range heaterGroupList {
						if htrGrp.GroupNo == htr.GroupNo {
							for htrID, grpHtr := range htrGrp.Heaters {
								if grpHtr.HeaterName == elem.HeaterName {
									heaterGroupList[grpID].Heaters[htrID].HeaterStatus = "off"
									break
								}
							}
							break
						}
					}
					break
				}
			}
			return
		}
	}
}

func writeMessageToClients(msg []byte) {
	isPrblm := false
	for _, client := range clients {
		err := client.WriteMessage(ws.TextMessage, msg)
		if err != nil {
			isPrblm = true
		}
	}

	if isPrblm == true {
		removeClient()
	}
}

func removeClient() {

	for index, client := range clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			clients = append(clients[:index], clients[index+1:]...)
			removeClient()
			break
		}
	}

	fmt.Println("Client Disconnected")
	fmt.Println("No. of Clients:", len(clients))
}
