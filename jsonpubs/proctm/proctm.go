package main

import (
	"Autobots/dbcon"
	"Autobots/heatwave/websocket"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"

	tm "Autobots/jsonpubs/proctm/ProcessedData"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	ws "github.com/gorilla/websocket"

	zmq "github.com/pebbe/zmq4"
)

type hkPacket struct {
	ParamID        string  `json:"param_id"`
	Parameter      string  `json:"param"`
	SourceInfo     string  `json:"source_info"`
	RawCount       int64   `json:"raw_count"`
	ProcValue      string  `json:"proc_value"`
	TimeStamp      string  `json:"time_stamp"`
	UpperLimit     float64 `json:"upper_limit"`
	LowerLimit     float64 `json:"lower_limit"`
	ErrDescription string  `json:"err_desc"`
}

type ptmPacket struct {
	Parameters     []string  `json:"params"`
	SourceInfo     string    `json:"source_info"`
	RawCount       []int64   `json:"raw_counts"`
	ProcValue      []string  `json:"proc_values"`
	TimeStamp      string    `json:"time_stamp"`
	UpperLimit     []float64 `json:"upper_limits"`
	LowerLimit     []float64 `json:"lower_limits"`
	ErrDescription string    `json:"err_desc"`
}

type dwellPacket struct {
	Parameters     []dwellPacketParamInfo `json:"params"`
	SourceInfo     string                 `json:"source_info"`
	TimeStamp      string                 `json:"time_stamp"`
	ErrDescription string                 `json:"err_desc"`
}

type dwellPacketParamInfo struct {
	Parameter  string   `json:"param"`
	RawCount   []int64  `json:"raw_count"`
	ProcValue  []string `json:"proc_value"`
	UpperLimit float64  `json:"upper_limit"`
	LowerLimit float64  `json:"lower_limit"`
}

type tmParam struct {
	pid      string
	mnemonic string
}

type clientInfo struct {
	connection    *ws.Conn
	subscriptions []string
	mutex         sync.Mutex
}

type subscriptionInfo struct {
	Action string   `json:"action"`
	Params []string `json:"params"`
}

var tm1Data map[string][]byte = make(map[string][]byte)
var tm2Data map[string][]byte = make(map[string][]byte)

var tmMnemonicList, trimmedMnemonicList map[string]string

var tm1Subscriber, tm2Subscriber, ptm1Subscriber, ptm2Subscriber, dwell1Subscriber, dwell2Subscriber *zmq.Socket

var serverPort, tm1Addr, tm2Addr *string

var ptm1Addr, ptm2Addr *string

var dwell1Addr, dwell2Addr *string

var chain1Clients, chain2Clients []*clientInfo

var chain1Subscriptions, chain2Subscriptions []string

var ptm1Clients, ptm2Clients, dwell1Clients, dwell2Clients []*ws.Conn

func main() {
	serverPort = flag.String("wstm1", "9050", "Server Publish Port")
	tm1Addr = flag.String("tm1", "127.0.0.1:5000", "TM-1 Publisher Address")
	tm2Addr = flag.String("tm2", "127.0.0.1:5001", "TM-2 Publisher Address")

	ptm1Addr = flag.String("ptm1", "127.0.0.1:5006", "PTM-1 Publisher Address")
	ptm2Addr = flag.String("ptm2", "127.0.0.1:5007", "PTM-2 Publisher Address")

	dwell1Addr = flag.String("dwell1", "127.0.0.1:5002", "Dwell-1 Publisher Address")
	dwell2Addr = flag.String("dwell2", "127.0.0.1:5003", "Dwell-2 Publisher Address")

	flag.Parse()

	dbcon.ConnectToDB()
	getTelemetryMnemonics()
	loadTMPages()

	go startTM1Publisher()
	go startTM2Publisher()
	go startPTM1Publisher()
	go startPTM2Publisher()
	go startDwell1Publisher()
	go startDwell2Publisher()

	r := mux.NewRouter()
	r.HandleFunc("/", homePage)
	r.HandleFunc("/pages", getTMPages)
	r.HandleFunc("/ws/tm1", wsTM1EndPoint)
	r.HandleFunc("/ws/tm2", wsTM2EndPoint)
	r.HandleFunc("/ws/ptm1", wsPTM1EndPoint)
	r.HandleFunc("/ws/ptm2", wsPTM2EndPoint)
	r.HandleFunc("/ws/dwell1", wsDwell1EndPoint)
	r.HandleFunc("/ws/dwell2", wsDwell2EndPoint)

	log.Fatal(http.ListenAndServe(":"+*serverPort, r))
}

func getTelemetryMnemonics() {
	result, err := dbcon.DBObject.Query("SELECT `PID`, `GCMnemonic` FROM `tm_parameters`")
	if err != nil {
		fmt.Println(err)
		return
	}

	reg, _ := regexp.Compile("[^a-zA-Z0-9+]+")

	var param tmParam
	tmMnemonicList = make(map[string]string)
	trimmedMnemonicList = make(map[string]string)

	for result.Next() {
		err := result.Scan(&param.pid, &param.mnemonic)
		if err != nil {
			fmt.Println(err)
		}

		tmMnemonicList[param.pid] = strings.TrimSpace(param.mnemonic)
		trimmedMnemonicList[param.pid] = strings.ToLower(reg.ReplaceAllString(param.mnemonic, ""))
	}
}

func startTM1Publisher() {
	protoPkt := &tm.HKPacket{}

	tm1Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer tm1Subscriber.Close()
	tm1Subscriber.Connect("tcp://" + *tm1Addr)

	tm1Subscriber.SetSubscribe("")

	for {

		jsonPkt := hkPacket{}

		contents, _ := tm1Subscriber.RecvBytes(0)

		proto.Unmarshal(contents, protoPkt)

		jsonPkt.ParamID = protoPkt.GetParamID()
		jsonPkt.Parameter = tmMnemonicList[protoPkt.GetParamID()]
		jsonPkt.RawCount = protoPkt.GetRawCount()
		jsonPkt.ProcValue = protoPkt.GetProcValue()
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.UpperLimit = protoPkt.GetUpperLimit()
		jsonPkt.LowerLimit = protoPkt.GetLowerLimit()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		tm1Data[jsonPkt.Parameter] = fmtJSON

		isPrblm := false

		for _, client := range chain1Clients {
			if paramExists(client.subscriptions, trimmedMnemonicList[protoPkt.GetParamID()]) || jsonPkt.ParamID == "Error" {
				client.mutex.Lock()
				err := client.connection.WriteMessage(ws.TextMessage, fmtJSON)
				client.mutex.Unlock()
				if err != nil {
					isPrblm = true
				}
			}
		}

		if isPrblm {
			removeChain1Client()
		}

	}

}

func startTM2Publisher() {
	protoPkt := &tm.HKPacket{}

	tm2Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer tm2Subscriber.Close()
	tm2Subscriber.Connect("tcp://" + *tm2Addr)

	tm2Subscriber.SetSubscribe("")

	reg, _ := regexp.Compile("[^a-zA-Z0-9+]+")

	for {

		jsonPkt := hkPacket{}

		contents, err := tm2Subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		jsonPkt.ParamID = protoPkt.GetParamID()
		jsonPkt.Parameter = tmMnemonicList[protoPkt.GetParamID()]
		jsonPkt.RawCount = protoPkt.GetRawCount()
		jsonPkt.ProcValue = protoPkt.GetProcValue()
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.UpperLimit = protoPkt.GetUpperLimit()
		jsonPkt.LowerLimit = protoPkt.GetLowerLimit()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		tm2Data[jsonPkt.Parameter] = fmtJSON

		isPrblm := false

		for _, client := range chain2Clients {

			if paramExists(client.subscriptions, strings.ToLower(reg.ReplaceAllString(jsonPkt.Parameter, ""))) || jsonPkt.ParamID == "Error" {
				client.mutex.Lock()
				err := client.connection.WriteMessage(ws.TextMessage, fmtJSON)
				client.mutex.Unlock()
				if err != nil {
					isPrblm = true
				}
			}

		}

		if isPrblm {
			removeChain2Client()
		}
	}

}

func startPTM1Publisher() {
	protoPkt := &tm.PTMPacket{}

	ptm1Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer ptm1Subscriber.Close()
	ptm1Subscriber.Connect("tcp://" + *ptm1Addr)

	ptm1Subscriber.SetSubscribe("")
	for {

		jsonPkt := ptmPacket{}

		contents, err := ptm1Subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		jsonPkt.Parameters = protoPkt.GetMnemonic()
		jsonPkt.RawCount = protoPkt.GetRawCount()
		jsonPkt.ProcValue = protoPkt.GetProcValue()
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.UpperLimit = protoPkt.GetUpperLimit()
		jsonPkt.LowerLimit = protoPkt.GetLowerLimit()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		isPrblm := false

		for _, client := range ptm1Clients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}
		}

		if isPrblm {
			removePTM1Client()
		}
	}
}

func startPTM2Publisher() {
	protoPkt := &tm.PTMPacket{}

	ptm2Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer ptm2Subscriber.Close()
	ptm2Subscriber.Connect("tcp://" + *ptm2Addr)

	ptm2Subscriber.SetSubscribe("")
	for {

		jsonPkt := ptmPacket{}

		contents, err := ptm2Subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		jsonPkt.Parameters = protoPkt.GetMnemonic()
		jsonPkt.RawCount = protoPkt.GetRawCount()
		jsonPkt.ProcValue = protoPkt.GetProcValue()
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.UpperLimit = protoPkt.GetUpperLimit()
		jsonPkt.LowerLimit = protoPkt.GetLowerLimit()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		isPrblm := false

		for _, client := range ptm2Clients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}
		}

		if isPrblm {
			removePTM2Client()
		}
	}
}

func startDwell1Publisher() {
	protoPkt := &tm.DwellPacket{}

	dwell1Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer dwell1Subscriber.Close()
	dwell1Subscriber.Connect("tcp://" + *dwell1Addr)

	dwell1Subscriber.SetSubscribe("")

	for {

		jsonPkt := dwellPacket{}

		contents, err := dwell1Subscriber.RecvBytes(0)

		// fmt.Println(contents)

		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		dwellParams := []dwellPacketParamInfo{}
		paramCount := len(protoPkt.GetParameters())

		for i := 0; i < paramCount; i++ {
			pktParam := dwellPacketParamInfo{}

			pktParam.Parameter = protoPkt.GetParameters()[i].GetMnemonic()
			pktParam.ProcValue = protoPkt.GetParameters()[i].GetProcValue()
			pktParam.RawCount = protoPkt.GetParameters()[i].GetRawCount()
			pktParam.UpperLimit = protoPkt.GetParameters()[i].GetUpperLimit()
			pktParam.LowerLimit = protoPkt.GetParameters()[i].GetLowerLimit()

			dwellParams = append(dwellParams, pktParam)
		}

		jsonPkt.Parameters = dwellParams
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		isPrblm := false

		for _, client := range dwell1Clients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}
		}

		if isPrblm {
			removeDwell1Client()
		}
	}
}

func startDwell2Publisher() {
	protoPkt := &tm.DwellPacket{}

	dwell2Subscriber, _ = zmq.NewSocket(zmq.SUB)
	defer dwell2Subscriber.Close()
	dwell2Subscriber.Connect("tcp://" + *dwell2Addr)

	dwell2Subscriber.SetSubscribe("")
	for {

		jsonPkt := dwellPacket{}

		contents, err := dwell2Subscriber.RecvBytes(0)
		if err != nil {
			fmt.Println(err)
		}

		proto.Unmarshal(contents, protoPkt)

		dwellParams := []dwellPacketParamInfo{}
		paramCount := len(protoPkt.GetParameters())

		for i := 0; i < paramCount; i++ {
			pktParam := dwellPacketParamInfo{}

			pktParam.Parameter = protoPkt.GetParameters()[i].GetMnemonic()
			pktParam.ProcValue = protoPkt.GetParameters()[i].GetProcValue()
			pktParam.RawCount = protoPkt.GetParameters()[i].GetRawCount()
			pktParam.UpperLimit = protoPkt.GetParameters()[i].GetUpperLimit()
			pktParam.LowerLimit = protoPkt.GetParameters()[i].GetLowerLimit()

			dwellParams = append(dwellParams, pktParam)
		}

		jsonPkt.Parameters = dwellParams
		jsonPkt.SourceInfo = protoPkt.GetSourceInfo()
		jsonPkt.TimeStamp = protoPkt.GetTimeStamp()
		jsonPkt.ErrDescription = protoPkt.GetErrDescription()

		fmtJSON, _ := json.Marshal(jsonPkt)
		isPrblm := false

		for _, client := range dwell2Clients {
			err := client.WriteMessage(ws.TextMessage, fmtJSON)
			if err != nil {
				isPrblm = true
			}
		}

		if isPrblm {
			removeDwell2Client()
		}
	}
}

func homePage(w http.ResponseWriter, r *http.Request) {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Fprintf(w, "Nothing Here! Go back %+v", ip)
}

type pageInfo struct {
	PageNo     int          `json:"page_no"`
	PageName   string       `json:"page_name"`
	PageDesc   string       `json:"page_desc"`
	PageParams []paramGroup `json:"page_groups"`
}

type paramGroup struct {
	Subtitle string   `json:"subtitle"`
	Params   []string `json:"params"`
	Units    []string `json:"units"`
}

var pages []pageInfo

func loadTMPages() {
	result, err := dbcon.DBObject.Query("SELECT `PageNumber`, `PageName`, `Description` FROM `PageDirectory` WHERE `TMFormatID` = 'HK'")
	if err != nil {
		fmt.Println(err)
		return
	}

	for result.Next() {
		var page pageInfo
		result.Scan(&page.PageNo, &page.PageName, &page.PageDesc)
		result1, _ := dbcon.DBObject.Query("SELECT `tm_par_name`, `subtitle` FROM `tm_page` WHERE `pageno` = '" + strconv.Itoa(page.PageNo) + "' AND `TMFormatID` = 'HK' ORDER BY `SNo`")

		var param string
		var subtitle string
		var units string
		var prevSubtitle string
		var paramgroup paramGroup
		for result1.Next() {
			result1.Scan(&param, &subtitle)

			if prevSubtitle != subtitle {
				page.PageParams = append(page.PageParams, paramgroup)
				paramgroup.Params = nil
				paramgroup.Units = nil
			}

			param = getActualMnemonic(param)

			paramgroup.Subtitle = strings.TrimSpace(subtitle)
			paramgroup.Params = append(paramgroup.Params, strings.TrimSpace(param))

			result2, _ := dbcon.DBObject.Query("SELECT `Units` FROM `tm_parameters` WHERE `GCMnemonic` = '" + param + "'")
			if result2.Next() {
				result2.Scan(&units)
			} else {
				units = ""
			}
			result2.Close()
			paramgroup.Units = append(paramgroup.Units, units)

			prevSubtitle = subtitle
		}

		result1.Close()

		page.PageParams = append(page.PageParams, paramgroup)

		pages = append(pages, page)
	}

	result.Close()
}

// getTMPages handles dynamic database updates in pages and page directory
func getTMPages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(pages)
}

func wsTM1EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	client := &clientInfo{}
	client.connection = conn

	chain1Clients = append(chain1Clients, client)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New client connected from", ip, "to Chain-1")
	fmt.Println("No. of Chain-1 Clients: ", len(chain1Clients))

	for {
		_, msg, err := conn.ReadMessage()

		if err != nil {
			log.Println(err)
			return
		}

		handleReceivedMessage(client, 1, msg)

	}
}

func wsTM2EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}

	client := &clientInfo{}
	client.connection = conn

	chain2Clients = append(chain2Clients, client)

	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	fmt.Println("New client connected from", ip, "to Chain-2")
	fmt.Println("No. of Chain-2 Clients: ", len(chain1Clients))

	for {
		_, msg, err := conn.ReadMessage()

		if err != nil {
			log.Println(err)
			return
		}

		handleReceivedMessage(client, 2, msg)

	}
}

func wsPTM1EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}
	ptm1Clients = append(ptm1Clients, conn)
}

func wsPTM2EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}
	ptm2Clients = append(ptm2Clients, conn)
}

func wsDwell1EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}
	dwell1Clients = append(dwell1Clients, conn)
}

func wsDwell2EndPoint(w http.ResponseWriter, r *http.Request) {
	conn, err := websocket.Upgrade(w, r)
	if err != nil {
		fmt.Fprintf(w, "%+v\n", err)
		return
	}
	dwell2Clients = append(dwell2Clients, conn)
}

func removeChain1Client() {
	for index, client := range chain1Clients {
		if err := client.connection.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			chain1Clients = append(chain1Clients[:index], chain1Clients[index+1:]...)
			removeChain1Client()
			break
		}
	}
}

func removeChain2Client() {
	for index, client := range chain2Clients {
		if err := client.connection.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			chain2Clients = append(chain2Clients[:index], chain2Clients[index+1:]...)
			removeChain2Client()
			break
		}
	}
}

func removePTM1Client() {
	for index, client := range ptm1Clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			ptm1Clients = append(ptm1Clients[:index], ptm1Clients[index+1:]...)
			removePTM1Client()
			break
		}
	}
}

func removePTM2Client() {
	for index, client := range ptm2Clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			ptm2Clients = append(ptm2Clients[:index], ptm2Clients[index+1:]...)
			removePTM2Client()
			break
		}
	}
}

func removeDwell1Client() {
	for index, client := range dwell1Clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			dwell1Clients = append(dwell1Clients[:index], dwell1Clients[index+1:]...)
			removeDwell1Client()
			break
		}
	}
}

func removeDwell2Client() {
	for index, client := range dwell2Clients {
		if err := client.WriteMessage(ws.PingMessage, []byte{}); err != nil {
			fmt.Println(err)
			dwell2Clients = append(dwell2Clients[:index], dwell2Clients[index+1:]...)
			removeDwell2Client()
			break
		}
	}
}

func handleReceivedMessage(client *clientInfo, chain int, msg []byte) {

	var jsonPkt subscriptionInfo
	err := json.Unmarshal(msg, &jsonPkt)
	if err != nil {
		fmt.Println("Received Wrong Packet:", err)
	}

	reg, _ := regexp.Compile("[^a-zA-Z0-9+]+")

	if jsonPkt.Action == "subscribe" {

		for i := 0; i < len(jsonPkt.Params); i++ {
			if jsonPkt.Params[i] == "" {
				client.subscriptions = append(client.subscriptions, "*")

			} else {
				param := strings.ToLower(reg.ReplaceAllString(jsonPkt.Params[i], ""))

				if _, ok := mapkey(trimmedMnemonicList, param); !ok {
					fmt.Println("Requested param does not exists in database:", param)
					continue
				}

				if !paramExists(client.subscriptions, param) {
					client.subscriptions = append(client.subscriptions, param)
				}

				if chain == 1 {
					client.mutex.Lock()
					client.connection.WriteMessage(ws.TextMessage, tm1Data[jsonPkt.Params[i]])
					client.mutex.Unlock()

					if !paramExists(chain1Subscriptions, param) {
						chain1Subscriptions = append(chain1Subscriptions, param)
					}

				} else if chain == 2 {
					client.mutex.Lock()
					client.connection.WriteMessage(ws.TextMessage, tm2Data[jsonPkt.Params[i]])
					client.mutex.Unlock()

					if !paramExists(chain2Subscriptions, param) {
						chain2Subscriptions = append(chain2Subscriptions, param)
					}

				}
			}

		}

	} else if jsonPkt.Action == "unsubscribe" {

		for i := 0; i < len(jsonPkt.Params); i++ {
			if jsonPkt.Params[i] == "" {
				client.subscriptions = nil
			} else {
				param := strings.ToLower(reg.ReplaceAllString(jsonPkt.Params[i], ""))

				index := getParamIndex(client.subscriptions, param)
				if index != -1 {
					client.subscriptions = append(client.subscriptions[:index], client.subscriptions[index+1:]...)
				} else {
					fmt.Println("Param not added for subscription:", param)
				}

				if chain == 1 {

					if !isParamFound(chain1Clients, param) {
						index := getParamIndex(chain1Subscriptions, param)
						if index != -1 {
							chain1Subscriptions = append(chain1Subscriptions[:index], chain1Subscriptions[index+1:]...)
						}
					}

				} else if chain == 2 {

					if !isParamFound(chain2Clients, param) {
						index := getParamIndex(chain2Subscriptions, param)
						if index != -1 {
							chain2Subscriptions = append(chain2Subscriptions[:index], chain2Subscriptions[index+1:]...)
						}
					}

				}
			}

		}

	} else {
		fmt.Println("Invalid Action Requested from Client:", jsonPkt.Action)
	}
}

func paramExists(list []string, value string) bool {
	for _, a := range list {
		if a == value || a == "*" {
			return true
		}
	}
	return false
}

func getParamIndex(list []string, value string) int {
	for index, a := range list {
		if a == value {
			return index
		}
	}
	return -1
}

func isParamFound(clientList []*clientInfo, value string) bool {
	for _, client := range clientList {
		paramList := client.subscriptions

		for _, a := range paramList {
			if a == value {
				return true
			}
		}
	}
	return false
}

func mapkey(m map[string]string, value string) (key string, ok bool) {
	for k, v := range m {
		if v == value {
			key = k
			ok = true
			return
		}
	}
	return
}

func getActualMnemonic(param string) string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9+]+")
	param = strings.ToLower(reg.ReplaceAllString(param, ""))

	paramID, ok := mapkey(trimmedMnemonicList, param)

	if ok {
		return tmMnemonicList[paramID]
	}

	return ""
}
