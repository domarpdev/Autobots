package globals

//Constants ...Structure for Global Constants
type Constants struct {
	NumHeaterGroups int
	NumHeaters      int
	NumTmpSensors   int
	NumPRT          int
}

// CurrNum ...Number for updating timer
var CurrNum int
