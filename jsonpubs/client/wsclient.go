package main

import (
	"encoding/json"
	"fmt"
	"net/url"

	ws "github.com/gorilla/websocket"
)

type scosPkt struct {
	ParamList []scosParamInfo `json:"params"`
	Stream    string          `json:"stream"`
	SeqCount  string          `json:"seqcount"`
	Time      string          `json:"time_stamp"`
	Error     string          `json:"err_desc"`
}

type scosParamInfo struct {
	Mnemonic string `json:"param"`
	Value    string `json:"value"`
}

var conn *ws.Conn

func main() {
	customURL := url.URL{Scheme: "ws", Host: "127.0.0.1:9012", Path: "/ws"}
	conn, _, _ = ws.DefaultDialer.Dial(customURL.String(), nil)

	// Go routine
	go handler()

	for {
		// _, msg, _ := conn.ReadMessage()
		// fmt.Println(string(msg))
	}
}

func handler() {
	for {
		// scosmsg := scosPkt{}
		var i interface{}

		_, msg, _ := conn.ReadMessage()

		json.Unmarshal(msg, &i)

		// json.Marshal(scosmsg)

		fmt.Println(i)
	}
}
